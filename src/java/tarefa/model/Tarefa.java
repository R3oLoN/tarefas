/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package tarefa.model;

import javax.persistence.Entity;
import javax.persistence.Id;

/**
 *
 * @author comp2
 */
@Entity
public class Tarefa {
    @Id
    private Long id;
    private String drescricao;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDrescricao() {
        return drescricao;
    }

    public void setDrescricao(String drescricao) {
        this.drescricao = drescricao;
    }
    
    
}
